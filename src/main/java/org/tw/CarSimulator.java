package org.tw;

import org.tw.error.EngineFailureError;
import org.tw.exception.FlatTyreException;
import org.tw.exception.FuelTankEmptyException;

import java.util.Random;

public class CarSimulator {
    public void setFlatTyre(boolean flatTyre) {
        isFlatTyre = flatTyre;
    }

    private boolean isFlatTyre;

    public void setEngineFailure(boolean engineFailure) {
        this.isEngineFailure = engineFailure;
    }

    private boolean isEngineFailure;

    public void setFuelTankEmpty(boolean fuelTankEmpty) {
        isFuelTankEmpty = fuelTankEmpty;
    }

    private boolean isFuelTankEmpty;

    public void drive() throws FuelTankEmptyException, FlatTyreException{
        System.out.println("Driving towards destination");
        if (isFuelTankEmpty) {
            throw new FuelTankEmptyException("Fuel tank is Empty");
        } else if (isFlatTyre) {
            throw new FlatTyreException("Tyre needs to be inflated");
        } else if (isEngineFailure) {
            throw new EngineFailureError("Engine Ignition failed");

        }
    }
}
