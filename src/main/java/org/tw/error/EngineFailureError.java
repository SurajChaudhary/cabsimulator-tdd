package org.tw.error;

public class EngineFailureError extends Error{
    public EngineFailureError(String message) {
        super(message);
    }
}

