package org.tw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.tw.error.EngineFailureError;
import org.tw.exception.FlatTyreException;
import org.tw.exception.FuelTankEmptyException;

import static org.junit.jupiter.api.Assertions.*;


public class CarSimulatorTest {
    CarSimulator carSimulator;

    @BeforeEach
    void setUp() {
        carSimulator = new CarSimulator();
    }

    @Test
    void shouldBeAbleToDriveWhenEngineStarts() {
        assertDoesNotThrow(() -> carSimulator.drive());
    }

    @Test
    void shouldNotBeAbleToDriveWhenNoFuel() {
        carSimulator.setFuelTankEmpty(true);
        assertThrows(FuelTankEmptyException.class, () -> carSimulator.drive());
    }


    @Test
    void shouldNotBeAbleToDriveWhenTyreIsFlat() {
        carSimulator.setFlatTyre(true);
        assertThrows(FlatTyreException.class, () -> carSimulator.drive());
    }

    @Test
    void shouldNotBeAbleToStartWhenEngineFailure() {
        carSimulator.setEngineFailure(true);
        assertThrows(EngineFailureError.class, () -> carSimulator.drive());
    }
}
