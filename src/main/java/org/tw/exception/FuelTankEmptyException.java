package org.tw.exception;

public class FuelTankEmptyException extends Exception{
    public FuelTankEmptyException(String message) {
        super(message);
    }
}
