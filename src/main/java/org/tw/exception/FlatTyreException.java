package org.tw.exception;

public class FlatTyreException extends Exception{
    public FlatTyreException(String message) {
        super(message);
    }
}
